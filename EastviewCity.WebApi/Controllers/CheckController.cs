﻿using EastviewCity.WebApi.Models.Check;
using EastviewCity.WebApi.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace EastviewCity.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CheckController : ControllerBase
    {
        private readonly ILogger<CheckController> _Logger;
        private CheckSettings _SettingsCheck;

        public CheckController(ILogger<CheckController> logger,
            IOptions<CheckSettings> settings
        )
        {
            _Logger = logger;
            _SettingsCheck = settings.Value;
        }

        [HttpGet]
        public ActionResult<CheckResponse> Get([FromQuery] CheckRequest request)
        {
            var result = new CheckResponse();
            result.Version = _SettingsCheck.Version;
            return result;
        }
    }
}