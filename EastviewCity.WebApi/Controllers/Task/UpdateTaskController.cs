﻿using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using EastviewCity.WebApi.Models.Person;
using EastviewCity.WebApi.Models.Task;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Task
{
    [ApiController]
    [Route("api/[controller]")]
    public class UpdateTaskController : ControllerBase
    {
        private readonly ILogger<UpdateTaskController> _Logger;
        private ITaskRepository _TaskRepo;

        public UpdateTaskController(ILogger<UpdateTaskController> logger, ITaskRepository taskRepository)
        {
            _Logger = logger;
            _TaskRepo = taskRepository;
        }

        [HttpPost]
        public async Task<ActionResult<UpdateTaskResponse>> PostAsync([FromBody] UpdateTaskRequest request)
        {
            if (!request.TaskId.HasValue) return BadRequest("Error en los requerimientos. El Id de la tarea no puede ser null");
            if (string.IsNullOrEmpty(request.Name)) return BadRequest("El Nombre es obligatorio");
            if (!request.Day.HasValue) return BadRequest("El dia es obligatorio");
            if (!request.PersonId.HasValue) return BadRequest("Error en los requerimientos. El Id de la persona no puede ser null");

            var taskDto = new TaskFullDto(){
                Id = request.TaskId.Value,
                Name = request.Name,
                Day = request.Day.Value,
                PersonId = request.PersonId.Value
            };

            var status = await _TaskRepo.UpdateTaskAsync(taskDto);
                
            return status ? Ok() : BadRequest("Error al actualizar la tarea");
        }

    }
}