﻿using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using EastviewCity.WebApi.Models.Task;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Task
{
    [ApiController]
    [Route("api/[controller]")]
    public class CreateTaskController : ControllerBase
    {
        private readonly ILogger<CreateTaskController> _Logger;
        private ITaskRepository _TaskRepo;

        public CreateTaskController(ILogger<CreateTaskController> logger, ITaskRepository taskRepository)
        {
            _Logger = logger;
            _TaskRepo = taskRepository;
        }

        [HttpPut]
        public async Task<ActionResult<CreateTaskResponse>> PutAsync([FromBody] CreateTaskRequest request)
        {
            if (string.IsNullOrEmpty(request.Name)) return BadRequest("La tarea es obligatoria");
            if (!request.Day.HasValue) return BadRequest("El dia es obligatorio");
            if (!request.PersonId.HasValue) return BadRequest("La persona es obligatoria");

            var taskDto = new TaskFullDto(){
                Name = request.Name,
                Day = request.Day.Value,
                PersonId = request.PersonId.Value
            };

            var status = await _TaskRepo.CreateTaskAsync(taskDto);
            
            return status ? Ok() : BadRequest("Error al crear la tarea");
        }

    }
}