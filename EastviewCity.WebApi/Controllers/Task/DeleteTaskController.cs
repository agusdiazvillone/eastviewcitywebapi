﻿using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using EastviewCity.WebApi.Models.Person;
using EastviewCity.WebApi.Models.Task;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Task
{
    [ApiController]
    [Route("api/[controller]")]
    public class DeleteTaskController : ControllerBase
    {
        private readonly ILogger<DeleteTaskController> _Logger;
        private ITaskRepository _TaskRepo;

        public DeleteTaskController(ILogger<DeleteTaskController> logger, ITaskRepository taskRepository)
        {
            _Logger = logger;
            _TaskRepo = taskRepository;
        }

        [HttpDelete]
        public async Task<ActionResult<DeleteTaskResponse>> DeleteAsync([FromQuery] DeleteTaskRequest request)
        {
            if (!request.TaskId.HasValue) return BadRequest("Error en los requerimientos. El Id no puede ser null");

            var status = await _TaskRepo.DeleteTaskAsync(request.TaskId.Value);
                
            return status ? Ok() : BadRequest("Error al borrar la tarea");
        }

    }
}