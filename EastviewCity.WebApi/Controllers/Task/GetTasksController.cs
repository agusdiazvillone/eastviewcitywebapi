﻿using System.Linq;
using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.WebApi.Models.Person;
using EastviewCity.WebApi.Models.Task;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Task
{
    [ApiController]
    [Route("api/[controller]")]
    public class GetTasksController : ControllerBase
    {
        private readonly ILogger<GetTasksController> _Logger;
        private ITaskRepository _TaskRepo;

        public GetTasksController(ILogger<GetTasksController> logger, ITaskRepository taskRepository)
        {
            _Logger = logger;
            _TaskRepo = taskRepository;
        }

        [HttpGet]
        public async Task<ActionResult<GetTaskResponse>> GetAsync([FromQuery] GetTaskRequest request)
        {
            var result = new GetTaskResponse();

            result.Tasks = await _TaskRepo.GetTasksAsync();
            
            return result;
        }
    }
}