﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using EastviewCity.WebApi.Models.Person;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Person
{
    [ApiController]
    [Route("api/[controller]")]
    public class CreatePersonController : ControllerBase
    {
        private readonly ILogger<CreatePersonController> _Logger;
        private IPersonRepository _PersonRepo;

        public CreatePersonController(ILogger<CreatePersonController> logger, IPersonRepository personRepository)
        {
            _Logger = logger;
            _PersonRepo = personRepository;
        }

        [HttpPut]
        public async Task<ActionResult<CreatePersonResponse>> PutAsync([FromBody] CreatePersonRequest request)
        {
            if (string.IsNullOrEmpty(request.Name)) return BadRequest("El Nombre es obligatorio");
            if (string.IsNullOrEmpty(request.FiscalKey)) return BadRequest("El RUT es obligatorio");

            var personDto = new PersonDto(){
                Name = request.Name,
                FiscalKey = request.FiscalKey
            };

            var status = await _PersonRepo.CreatePersonAsync(personDto);
            
            return status ? Ok() : BadRequest("Error al crear la persona");
        }

    }
}