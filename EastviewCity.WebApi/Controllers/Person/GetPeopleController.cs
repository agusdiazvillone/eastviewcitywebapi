﻿using System.Linq;
using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.WebApi.Models.Person;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Person
{
    [ApiController]
    [Route("api/[controller]")]
    public class GetPeopleController : ControllerBase
    {
        private readonly ILogger<GetPeopleController> _Logger;
        private IPersonRepository _PersonRepo;

        public GetPeopleController(ILogger<GetPeopleController> logger, IPersonRepository personRepository)
        {
            _Logger = logger;
            _PersonRepo = personRepository;
        }

        [HttpGet]
        public async Task<ActionResult<GetPersonResponse>> GetAsync([FromQuery] GetPersonRequest request)
        {
            var result = new GetPersonResponse();

            result.People = await _PersonRepo.GetPeopleAsync();
            
            return result;
        }
    }
}