﻿using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using EastviewCity.WebApi.Models.Person;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Person
{
    [ApiController]
    [Route("api/[controller]")]
    public class DeletePersonController : ControllerBase
    {
        private readonly ILogger<DeletePersonController> _Logger;
        private IPersonRepository _PersonRepo;

        public DeletePersonController(ILogger<DeletePersonController> logger, IPersonRepository personRepository)
        {
            _Logger = logger;
            _PersonRepo = personRepository;
        }

        [HttpDelete]
        public async Task<ActionResult<DeletePersonResponse>> DeleteAsync([FromQuery] DeletePersonRequest request)
        {
            if (!request.PersonId.HasValue) return BadRequest("Error en los requerimientos. El Id no puede ser null");

            var status = await _PersonRepo.DeletePersonAsync(request.PersonId.Value);
                
            return status ? Ok() : BadRequest("Error al borrar la persona");
        }

    }
}