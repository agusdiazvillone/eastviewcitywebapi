﻿using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using EastviewCity.WebApi.Models.Person;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.Person
{
    [ApiController]
    [Route("api/[controller]")]
    public class UpdatePersonController : ControllerBase
    {
        private readonly ILogger<UpdatePersonController> _Logger;
        private IPersonRepository _PersonRepo;

        public UpdatePersonController(ILogger<UpdatePersonController> logger, IPersonRepository personRepository)
        {
            _Logger = logger;
            _PersonRepo = personRepository;
        }

        [HttpPost]
        public async Task<ActionResult<UpdatePersonResponse>> PostAsync([FromBody] UpdatePersonRequest request)
        {
            if (!request.PersonId.HasValue) return BadRequest("Error en los requerimientos. El Id no puede ser null");
            if (string.IsNullOrEmpty(request.Name)) return BadRequest("El Nombre es obligatorio");
            if (string.IsNullOrEmpty(request.FiscalKey)) return BadRequest("El RUT es obligatorio");

            var personDto = new PersonDto(){
                Id = request.PersonId.Value,
                Name = request.Name,
                FiscalKey = request.FiscalKey
            };

            var status = await _PersonRepo.UpdatePersonAsync(personDto);
                
            return status ? Ok() : BadRequest("Error al actualizar la persona");
        }

    }
}