﻿using System.Threading.Tasks;
using EastviewCity.Model;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.WebApi.Models.PersonTasks;
using EastviewCity.WebApi.Models.Task;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Controllers.PersonTasks
{
    [ApiController]
    [Route("api/[controller]")]
    public class GetPersonTasksByDayController : ControllerBase
    {
        private readonly ILogger<GetPersonTasksByDayController> _Logger;
        private ITaskRepository _TaskRepo;

        public GetPersonTasksByDayController(ILogger<GetPersonTasksByDayController> logger, ITaskRepository taskRepository)
        {
            _Logger = logger;
            _TaskRepo = taskRepository;
        }

        [HttpGet]
        public async Task<ActionResult<GetPersonTasksByDayResponse>> GetAsync([FromQuery] GetPersonTasksByDayRequest request)
        {
            var result = new GetPersonTasksByDayResponse();

            if (!request.Day.HasValue) return BadRequest("El dia es obligatorio");

            result.People = await _TaskRepo.GetPersonTasksByDayAsync((Day)request.Day.Value);

            return result;
        }
    }
}