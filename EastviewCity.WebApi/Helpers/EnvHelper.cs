﻿using Microsoft.Extensions.Configuration;
using System;

namespace EastviewCity.WebApi.Helpers
{
    public static class EnvHelper
    {
        public static string GetVal(IConfiguration conf, string key)
        {
            var value = Environment.GetEnvironmentVariable(key);
            if (String.IsNullOrEmpty(value))
            {
                value = conf[key];

            }

            return value;
        }

        public static string GetValue(this IConfiguration conf, string key)
        {
            var value = Environment.GetEnvironmentVariable(key);
            if (String.IsNullOrEmpty(value))
            {
                value = conf[key];

            }

            return value;
        }
    }
}
