using System.Collections.Generic;
using System.Globalization;
using EastviewCity.EFCore;
using EastviewCity.Repositories.EFCore;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.WebApi.Helpers;
using EastviewCity.WebApi.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EastviewCity.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _Configuration = configuration;
        }

        public IConfiguration _Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EastviewCityDbContext>(o =>
            {
                o.UseSqlServer(_Configuration.GetConnectionString("MainConnection"));
            });
            //services.AddDbContext<EastviewCityDbContext>(ServiceLifetime.Transient);

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddControllers();

            FillRepositoriesDependencyInjectionSetup(services);
            FillSettingsDependencyInjectionSetup(services);
        }

        private void FillSettingsDependencyInjectionSetup(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<CheckSettings>(options => options.Version = EnvHelper.GetVal(_Configuration, "Version"));
        }

        private void FillRepositoriesDependencyInjectionSetup(IServiceCollection services)
        {
            services.AddTransient<IPersonRepository, PersonRepository<EastviewCityDbContext>>();
            services.AddTransient<ITaskRepository, TaskRepository<EastviewCityDbContext>>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("MyPolicy");
            app.UseRouting();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var defaultDateCulture = "es-AR";
            var ci = new CultureInfo(defaultDateCulture);
            ci.NumberFormat.NumberDecimalSeparator = ",";
            ci.NumberFormat.CurrencyDecimalSeparator = ",";

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(ci),
                SupportedCultures = new List<CultureInfo>
                {
                    ci,
                },
                SupportedUICultures = new List<CultureInfo>
                {
                    ci,
                }
            });
        }
    }
}
