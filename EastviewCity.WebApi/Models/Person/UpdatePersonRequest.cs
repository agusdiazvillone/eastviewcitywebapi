namespace EastviewCity.WebApi.Models.Person
{
    public class UpdatePersonRequest
    {
        public int? PersonId { get; set; }        
        public string Name { get; set; }        
        public string FiscalKey { get; set; }
    }
}