namespace EastviewCity.WebApi.Models.Person
{
    public class CreatePersonRequest
    {
        public string Name { get; set; }        
        public string FiscalKey { get; set; }
    }
}