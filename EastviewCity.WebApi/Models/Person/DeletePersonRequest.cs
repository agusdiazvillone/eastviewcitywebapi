namespace EastviewCity.WebApi.Models.Person
{
    public class DeletePersonRequest
    {
        public int? PersonId { get; set; }
    }
}