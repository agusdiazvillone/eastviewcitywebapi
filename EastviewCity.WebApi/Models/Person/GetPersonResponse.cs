using System.Collections.Generic;
using EastviewCity.Repositories.Interfaces.Models.Person;

namespace EastviewCity.WebApi.Models.Person
{
    public class GetPersonResponse
    {
        public GetPersonResponse()
        {
        }

        public List<PersonDto> People { get; set; }
    }
}