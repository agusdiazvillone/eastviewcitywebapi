using System.Collections.Generic;
using EastviewCity.Repositories.Interfaces.Models.Person;

namespace EastviewCity.WebApi.Models.PersonTasks
{
    public class GetPersonTasksByDayResponse
    {
        public GetPersonTasksByDayResponse()
        {
        }

        public List<PersonDto> People { get; set; }

    }
}