namespace EastviewCity.WebApi.Models.PersonTasks
{
    public class GetPersonTasksByDayRequest
    {
        public int? Day { get; set; }
    }
}