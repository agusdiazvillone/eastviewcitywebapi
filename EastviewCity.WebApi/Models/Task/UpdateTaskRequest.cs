using EastviewCity.Model;

namespace EastviewCity.WebApi.Models.Task
{
    public class UpdateTaskRequest
    {
        public int? TaskId { get; set; }
        public string Name { get; set; }
        public Day? Day { get; set; }
        public int? PersonId { get; set; }
    }
}