namespace EastviewCity.WebApi.Models.Task
{
    public class DeleteTaskRequest
    {
        public int? TaskId { get; set; }
    }
}