using EastviewCity.Model;

namespace EastviewCity.WebApi.Models.Task
{
    public class CreateTaskRequest
    {
        public string Name { get; set; }
        public Day? Day { get; set; }
        public int? PersonId { get; set; }
    }
}