using System.Collections.Generic;
using EastviewCity.Repositories.Interfaces.Models.Person;

namespace EastviewCity.WebApi.Models.Task
{
    public class GetTaskResponse
    {
        public GetTaskResponse()
        {
        }

        public List<TaskFullDto> Tasks { get; set; }

    }
}