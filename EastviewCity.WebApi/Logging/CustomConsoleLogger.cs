﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;

namespace EastviewCity.WebApi.Logging
{
    public class CustomConsoleLogger : ILogger
    {
        private readonly string _categoryName;

        public CustomConsoleLogger(string categoryName)
        {
            _categoryName = categoryName;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss.fff")} {logLevel}: [ThreadId={Thread.CurrentThread.ManagedThreadId}] {_categoryName}: {formatter(state, exception)}");
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
    }
}
