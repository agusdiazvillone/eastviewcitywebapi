﻿using Microsoft.Extensions.Logging;

namespace EastviewCity.WebApi.Logging
{
    public class CustomLoggerProvider : ILoggerProvider
    {
        public void Dispose() { }

        public ILogger CreateLogger(string categoryName)
        {
            return new CustomConsoleLogger(categoryName);
        }
    }
}
