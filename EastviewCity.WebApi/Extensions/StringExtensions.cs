namespace EastviewCity.WebApi.Extensions
{
    public static class StringExtensions
    {
          public static string ControllerUrlName(this string val)
        {
            if (val == null) return val;
            else return val.Replace("Controller", "");
        }
    }
}