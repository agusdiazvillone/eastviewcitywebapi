IMAGE_NAME=eastviewcity-webapi
VERSION=1.0.0

docker: 
	json -I -f EastviewCity.WebApi/appsettings.json -e 'this.Version="$(VERSION)"' 
	sudo docker build -f Dockerfile -t $(IMAGE_NAME) .
	sudo docker tag $(IMAGE_NAME):latest $(IMAGE_NAME):$(VERSION)

exec_docker:
	sudo docker run -p 5000:5000 \
    -e "ConnectionStrings:MainConnection=Server=localhost;Database=EastviewCity;Trusted_Connection=True;MultipleActiveResultSets=true" \
	$(IMAGE_NAME)

ngrok:
	../../Documentos/ngrok/ngrok http 5000 --host-header localhost
