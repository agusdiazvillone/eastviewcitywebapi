﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EastviewCity.Model;
using EastviewCity.Repositories.Interfaces.Models.Person;

namespace EastviewCity.Repositories.Interfaces
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        Task<List<PersonDto>> GetPeopleAsync();
        Task<bool> CreatePersonAsync(PersonDto personDto);
        Task<bool> UpdatePersonAsync(PersonDto personDto);
        Task<bool> DeletePersonAsync(int personId);
    }
}
