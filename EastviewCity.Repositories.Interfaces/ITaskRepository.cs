﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EastviewCity.Model;
using EastviewCity.Repositories.Interfaces.Models.Person;

namespace EastviewCity.Repositories.Interfaces
{
    public interface ITaskRepository : IGenericRepository<Model.Task>
    {
        Task<bool> CreateTaskAsync(TaskFullDto taskDto);
        Task<bool> DeleteTaskAsync(int taskId);
        Task<bool> UpdateTaskAsync(TaskFullDto taskDto);
        Task<List<TaskFullDto>> GetTasksAsync();
        Task<List<PersonDto>> GetPersonTasksByDayAsync(Day day);
    }
}
