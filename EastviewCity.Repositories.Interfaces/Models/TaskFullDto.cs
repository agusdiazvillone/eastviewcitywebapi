using EastviewCity.Model;

namespace EastviewCity.Repositories.Interfaces.Models.Person
{
    public class TaskFullDto
    {
        public TaskFullDto()
        {
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public Day Day { get; set; }
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public string PersonFiscalKey { get; set; }
    }
}