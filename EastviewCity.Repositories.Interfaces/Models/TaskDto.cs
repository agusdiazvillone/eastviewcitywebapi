using EastviewCity.Model;

namespace EastviewCity.Repositories.Interfaces.Models.Person
{
    public class TaskDto
    {
        public TaskDto()
        {
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public Day Day { get; set; }
    }
}