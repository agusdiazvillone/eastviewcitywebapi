using System.Collections.Generic;

namespace EastviewCity.Repositories.Interfaces.Models.Person
{
    public class PersonDto
    {
        public PersonDto()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string FiscalKey { get; set; }
        public List<TaskDto> Tasks { get; set; }
    }
}