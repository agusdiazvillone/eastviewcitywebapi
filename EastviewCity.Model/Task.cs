﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EastviewCity.Model
{
    public class Task
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public Day Day { get; set; }
        public int RelatedPersonId { get; set; }
        public virtual Person RelatedPerson { get; set; }
    }
}
