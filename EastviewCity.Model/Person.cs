﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EastviewCity.Model
{
    public class Person
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string FiscalKey { get; set; }
        public List<Task> RelatedTasks { get; set; }
    }
}
