FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build

WORKDIR /code

COPY . .

RUN dotnet publish EastviewCity.WebApi --output /output --configuration Release

FROM mcr.microsoft.com/dotnet/aspnet:5.0-focal

COPY --from=build /output /app

ENV ASPNETCORE_URLS=http://+:5000

EXPOSE 5000

RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1/g' /etc/ssl/openssl.cnf

RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1/g' /usr/lib/ssl/openssl.cnf

WORKDIR /app

ENTRYPOINT ["dotnet", "EastviewCity.WebApi.dll"]