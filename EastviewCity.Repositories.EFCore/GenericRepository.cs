﻿using EastviewCity.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EastviewCity.Repositories.EFCore
{
    public class GenericRepository<TEntity, TDbContext> : IGenericRepository<TEntity> where TEntity : class where TDbContext : DbContext
    {
        protected TDbContext _Context;
        protected DbSet<TEntity> _DbSet;

        public GenericRepository(TDbContext context)
        {
            this._Context = context;
            this._DbSet = _Context.Set<TEntity>();
        }

    }
}
