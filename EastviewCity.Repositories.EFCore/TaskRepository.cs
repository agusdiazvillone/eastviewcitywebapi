using System.Collections.Generic;
using System.Threading.Tasks;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using EastviewCity.Model;

namespace EastviewCity.Repositories.EFCore
{
    public class TaskRepository<TDbContext> : GenericRepository<Model.Task, TDbContext>, ITaskRepository where TDbContext : DbContext
    {
        public TaskRepository(TDbContext context) : base(context)
        {

        }

        public async Task<bool> CreateTaskAsync(TaskFullDto taskDto)
        {
            try
            {
                var task = new Model.Task(){
                    Name = taskDto.Name,
                    Day = taskDto.Day,
                    RelatedPersonId = taskDto.PersonId
                };

                await _DbSet.AddAsync(task);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteTaskAsync(int taskId)
        {
            try
            {
                var task = await _DbSet.FindAsync(taskId);
            
                if (task == null) return false;

                _DbSet.Remove(task);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public async Task<List<PersonDto>> GetPersonTasksByDayAsync(Day day)
        {
            var resp = new List<PersonDto>();

            // resp = await (from t in _DbSet.AsQueryable().Include("RelatedPerson")
            //     where t.Day == day
            //     select t
            //     ).GroupBy(t => new {t.RelatedPersonId, t.RelatedPerson.Id, t.RelatedPerson.FiscalKey, t.RelatedPerson.Name})
            //     .Select(t => new PersonDto(){
            //         Id = t.Key.Id,
            //         Name = t.Key.Name,
            //         FiscalKey = t.Key.FiscalKey,
            //         Tasks = t.Select(x => new TaskDto(){
            //             Id = x.Id,
            //             Day = x.Day,
            //             Name = x.Name
            //         }).ToList()
            //     }).ToListAsync();

            var tasks = await (from t in _DbSet.AsQueryable().Include("RelatedPerson")
            where t.Day == day
            select t)
            .ToListAsync();

            resp = tasks.GroupBy(t => new {t.RelatedPersonId, t.RelatedPerson.Id, t.RelatedPerson.FiscalKey, t.RelatedPerson.Name})
                .Select(t => new PersonDto(){
                    Id = t.Key.Id,
                    Name = t.Key.Name,
                    FiscalKey = t.Key.FiscalKey,
                    Tasks = t.Select(x => new TaskDto(){
                        Id = x.Id,
                        Day = x.Day,
                        Name = x.Name
                    }).ToList()
                }).ToList();

            return resp;
        }

        public async Task<List<TaskFullDto>> GetTasksAsync()
        {
            var resp = await (from t in _DbSet.AsQueryable().Include("RelatedPerson")
                    select new TaskFullDto(){
                        Id = t.Id,
                        Name = t.Name,
                        Day = t.Day,
                        PersonId = t.RelatedPersonId,
                        PersonName = t.RelatedPerson.Name,
                        PersonFiscalKey = t.RelatedPerson.FiscalKey
                    }).OrderBy(t => t.Name).ThenBy(t => t.Day).ToListAsync();

            return resp;
        }

        public async Task<bool> UpdateTaskAsync(TaskFullDto taskDto)
        {
            try
            {
                var task = await _DbSet.FindAsync(taskDto.Id);
            
                if (task == null) return false;

                task.Name = taskDto.Name;
                task.Day = taskDto.Day;
                task.RelatedPersonId = taskDto.PersonId;

                _DbSet.Update(task);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        
    }
}
