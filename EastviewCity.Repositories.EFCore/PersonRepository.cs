using System.Collections.Generic;
using System.Threading.Tasks;
using EastviewCity.Model;
using EastviewCity.Repositories.Interfaces;
using EastviewCity.Repositories.Interfaces.Models.Person;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace EastviewCity.Repositories.EFCore
{
    public class PersonRepository<TDbContext> : GenericRepository<Person, TDbContext>, IPersonRepository where TDbContext : DbContext
    {
        public PersonRepository(TDbContext context) : base(context)
        {
        
        }

        public async Task<bool> CreatePersonAsync(PersonDto personDto)
        {
            try
            {
                var person = new Person(){
                    Name = personDto.Name,
                    FiscalKey = personDto.FiscalKey
                };

                await _DbSet.AddAsync(person);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeletePersonAsync(int personId)
        {
            try
            {
                var person = await _DbSet.FindAsync(personId);
            
                if (person == null) return false;

                _DbSet.Remove(person);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public async Task<List<PersonDto>> GetPeopleAsync()
        {
            var resp = await (from p in _DbSet.AsQueryable()
                    select new PersonDto(){
                        Id = p.Id,
                        Name = p.Name,
                        FiscalKey = p.FiscalKey
                    }).OrderBy(t => t.Name).ToListAsync();

            return resp;
        }

        public async System.Threading.Tasks.Task<bool> UpdatePersonAsync(PersonDto personDto)
        {
            try
            {
                var person = await _DbSet.FindAsync(personDto.Id);
            
                if (person == null) return false;

                person.Name = personDto.Name;
                person.FiscalKey = personDto.FiscalKey;

                _DbSet.Update(person);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

    }
}
