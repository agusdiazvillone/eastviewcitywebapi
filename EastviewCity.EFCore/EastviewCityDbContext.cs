﻿using System;
using System.IO;
using EastviewCity.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace EastviewCity.EFCore
{
    public class EastviewCityDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Task> Tasks { get; set; }

        public EastviewCityDbContext(DbContextOptions<EastviewCityDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(bc => bc.RelatedPerson)
                .WithMany()
                .HasForeignKey(bc => bc.RelatedPersonId);

            modelBuilder.Entity<Person>()
                .HasMany(bc => bc.RelatedTasks)
                .WithOne(e => e.RelatedPerson)
                .HasForeignKey(e => e.RelatedPersonId)
                .IsRequired();
        }

    }
}